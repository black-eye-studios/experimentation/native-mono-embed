#include "MonoRuntime.h"

#include <shlwapi.h> // PathRemoveFileSpecA

namespace Pls
{
    /*---------------------------------------------------------------------------------*/
    /* Constructors/Destructor                                                         */
    /*---------------------------------------------------------------------------------*/
    MonoRuntime::MonoRuntime()
    {
        // Get the current executable directory
        runtimePath.resize(MAX_PATH, '\0');
        GetModuleFileNameA(nullptr, runtimePath.data(), MAX_PATH);
        PathRemoveFileSpecA(runtimePath.data()); 
        // Since PathRemoveFileSpecA() removes from data(), the size is not updated, so we must manually update it
        runtimePath.resize(std::strlen(runtimePath.data())); 

        // Construct the lib, etc and DLL path
        std::string libPath(runtimePath);
        libPath += "\\mono\\lib";
        std::string etcPath(runtimePath);
        etcPath += "\\mono\\etc";
        std::string dllPath(runtimePath);
        dllPath += "\\ManagedLibrary.dll";

        // Set up library directories (only accepts absolute directories)
        mono_set_dirs(libPath.c_str(), etcPath.c_str());

        // Create the main CSharp domain
        domain = mono_jit_init("CSharpDomain");
        if (!domain)
        {
            throw SystemInitException("[MonoRuntime] Failed to intiialize Mono Runtime!");
        }
    }
    MonoRuntime::~MonoRuntime()
    {
        mono_jit_cleanup(domain);
    }
    /*---------------------------------------------------------------------------------*/
    /* Usage Functions                                                                 */
    /*---------------------------------------------------------------------------------*/
    MonoObject* MonoRuntime::CreateObject(const std::string& assemblyName, 
                                          const std::string& namespaceName,
                                          const std::string& className)
    {
        MonoImage* image = getImage(assemblyName);

        // Get an object that describes the class
        MonoClass* monoClass = mono_class_from_name(image, namespaceName.c_str(), className.c_str());
        if (!monoClass)
        {
            std::ostringstream oss;
            oss << "[MonoRuntime] Failed to get MonoClass that describes \"" << namespaceName << "." << className << "\" in assembly (" << assemblyName << "). \n";
            throw ObjectCreateFailedException(oss.str());
        }
        // Create the object
        MonoObject* obj = mono_object_new(domain, monoClass);
        if (!obj)
        {
            std::ostringstream oss;
            oss << "[MonoRuntime] Failed to create object of type \"" << namespaceName << "." << className << "\" in assembly (" << assemblyName << "). \n";
            throw ObjectCreateFailedException(oss.str());
        }
        // - Construct the object
        mono_runtime_object_init(obj);

        return obj;
    }
    /*---------------------------------------------------------------------------------*/
    /* Helper Functions                                                                */
    /*---------------------------------------------------------------------------------*/
    MonoImage* MonoRuntime::getImage(const std::string& assemblyName)
    {
        // Get path to the assembly
        std::string dllPath(runtimePath);
        dllPath += "\\" + assemblyName + ".dll";
        // Load the binary file as an Assembly
        MonoAssembly* csharpAssembly = mono_domain_assembly_open(domain, dllPath.c_str());
        if (!csharpAssembly)
        {
            std::ostringstream oss;
            oss << "[MonoRuntime] Failed to load the \"" << assemblyName << "\" assembly. \n";
            throw AssemblyLoadException(oss.str());
        }

        MonoImage* image = mono_assembly_get_image(csharpAssembly);
        if (!image)
        {
            std::ostringstream oss;
            oss << "[MonoRuntime] Failed to get an MonoImage of the \"" << assemblyName << "\" assembly. \n";
            throw AssemblyLoadException(oss.str());
        }
        return image;
    }
}