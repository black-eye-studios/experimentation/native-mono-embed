#include <iostream>

#include "MonoRuntime.h"


int main()
{
    Pls::MonoRuntime mono;

    using blankFunc = void(*)(MonoException**);
    blankFunc staticPrint =  mono.GetFunctionPtr<blankFunc>
    (
        "ManagedLibrary",
        "ManagedWorker:StaticPrintFunction()"
    );
    MonoException* exp;
    staticPrint(&exp);

    using blankInstFunc = void(*)(MonoObject*, MonoException**);
    blankInstFunc staticInstPrint =  mono.GetFunctionPtr<blankInstFunc>
    (
        "ManagedLibrary",
        "ManagedWorker:InstancedPrintFunction()"
    );
    MonoObject* obj = mono.CreateObject("ManagedLibrary", "ManagedLibrary", "ManagedWorker");
    staticInstPrint(obj, &exp);

    return 0;
}