#pragma once

#include <iostream>
#include <iomanip>          // std::setfill, std::setw
#include <stdexcept>        // std::runtime_error
#include <string>           // std::string
#include <sstream>          // std::ostringstream
#include <Windows.h>        // HMODULE
// Mono
#include <jit/jit.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/debug-helpers.h>


#include "Exceptions.h"     // FunctionPtrNotFoundException

namespace Pls
{
    /********************************************************************************//*!
    @brief    Class that encapsulates the state of the Mono Runtime lifecycle.
    *//*********************************************************************************/
    class MonoRuntime
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructor                                                     */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Initializes the Mono Runtime. Sets up directories for Mono and 
                  initializes the domain.
        *//*****************************************************************************/
        MonoRuntime();
        /****************************************************************************//*!
        @brief    Unloads the Mono Runtime.
        *//*****************************************************************************/
        ~MonoRuntime();

        // Disallow copy and moving
        MonoRuntime(const MonoRuntime&) = delete;
        MonoRuntime(MonoRuntime&&) = delete;

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a function pointer from the a CLR assembly based on the 
                  specified assembly, type and function names.

        @tparam        FunctionType
                Type of the function pointer that the specified function name will 
                provide.
                
        @params[in]    assemblyName
                Name of the CLR assembly that contains the function.
        @params[in]    functionName
                Name of the CLR function to get a pointer to.

        @returns  Pointer to the function in the assembly that was specified.
        *//*****************************************************************************/
        template<typename FunctionType>
        FunctionType GetFunctionPtr(const std::string& assemblyName, 
                                    const std::string& functionName);
        /****************************************************************************//*!
        @brief    Creates a CLR object of a specific type based on the specified 
                  assembly, type and function names.

                
        @params[in]    assemblyName
                Name of the CLR assembly that contains the class of the object to create.
        @params[in]    namespaceName
                Name of the namespace that contains the type of the object to create.
                Nested types are separated by a period(.).
        @params[in]    className
                Name of the CLR class to create.

        @returns  Pointer to a MonoObject that represents the created object.
        *//*****************************************************************************/
        MonoObject* CreateObject(const std::string& assemblyName, 
                                 const std::string& namespaceName, 
                                 const std::string& className);

      private:
        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        MonoDomain* domain = nullptr;
        std::string runtimePath;

        /*-----------------------------------------------------------------------------*/
        /* Helper Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Loads a MonoImage that describes a specified Assembly for use with
                  other functions.

                
        @params[in]    assemblyName
                Name of the CLR assembly to load an image of.

        @returns  Pointer to a MonoImage that represents the specified assembly.
        *//*****************************************************************************/
        /*-----------------------------------------------------------------------------*/
        MonoImage* getImage(const std::string& assemblyName);
    };

    /*---------------------------------------------------------------------------------*/
    /* Template Function Implementations                                               */
    /*---------------------------------------------------------------------------------*/
    template<typename FunctionType>
    FunctionType MonoRuntime::GetFunctionPtr(const std::string& assemblyName, 
                                             const std::string& functionName)
    {
        // Get the assembly image to search
        MonoImage* image = getImage(assemblyName);

        // Create a description of the method
        MonoMethodDesc* desc = mono_method_desc_new (functionName.c_str(), false);
        if (!desc)
        {
            std::ostringstream oss;
            oss << "[MonoRuntime] Failed to create MonoMethodDesc that describes \"" << functionName << "\" in assembly (" << assemblyName << "). \n";
            throw FunctionPtrNotFoundException(oss.str());
        }

        // Get the method
        MonoMethod* method = mono_method_desc_search_in_image (desc, image);
        if (!method)
        {
            std::ostringstream oss;
            oss << "[MonoRuntime] Failed to get MonoMethod that describes \"" << functionName << "\" in assembly (" << assemblyName << "). \n";
            throw FunctionPtrNotFoundException(oss.str());
        }

        // Free the description
        mono_method_desc_free (desc);

        // Get the Thunk
        FunctionType managedDelegate = reinterpret_cast<FunctionType>(mono_method_get_unmanaged_thunk(method));

        // Check if it failed
        if (!managedDelegate)
        {
            std::ostringstream oss;
            oss << "[MonoRuntime] Failed to get pointer to function \"" << functionName << "\" in assembly (" << assemblyName << "). \n";
            throw FunctionPtrNotFoundException(oss.str());
        }

        return managedDelegate;
    }
}

