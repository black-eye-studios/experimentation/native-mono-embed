﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagedLibrary
{
    public class ManagedWorker
    {
        public static void StaticPrintFunction()
        {
            Console.WriteLine("Static Hello World!");
        }
        public void InstancedPrintFunction()
        {
            Console.WriteLine("Instanced Hello World!");
        }
    }
}
